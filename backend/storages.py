import pymongo
import redis


class MongoSource(object):

	mongo_src = None

	@classmethod
	def setup(cls, settings):
		connection = pymongo.MongoClient("localhost", 27017)
		cls.mongo_src = connection.test


class RedisSource(object):

	redis_src = None

	@classmethod
	def setup(cls, settings):
		cls.redis_src = redis.Redis()


import tornado.web
import tornado.escape
import tornado.auth

from backend.storages import MongoSource
from backend.storages import RedisSource


class BaseHandler(tornado.web.RequestHandler, MongoSource, RedisSource):

    def get_current_user(self):
        user_json = self.get_secure_cookie("user")
        if not user_json:
        	return None
        return tornado.escape.json_decode(user_json)


class AuthHandler(BaseHandler):

    def get(self):
        if self.get_current_user():
            self.write('already authenticated')
        else:
            self.render("accounts/auth.html")


class FacebookAuthHandler(BaseHandler, tornado.auth.FacebookGraphMixin):

    @tornado.web.asynchronous
    def get(self):
        url = self.request.protocol + "://" + self.request.host + self.reverse_url("auth_facebook")
        if self.get_argument("code", False):
            self.get_authenticated_user(
                redirect_uri=url,
                client_id=self.settings["facebook_api_key"],
                client_secret=self.settings["facebook_secret"],
                code=self.get_argument("code"),
                callback=self.async_callback(self._on_auth)
            )
            return
        return self.authorize_redirect(redirect_uri=url,
                          client_id=self.settings["facebook_api_key"],
                          extra_params={"scope": "read_stream,offline_access"})

    def _on_auth(self, user):
        if not user:
            raise tornado.web.HTTPError(500, "Facebook auth failed")
        user["auth"] = "facebook"
        print user
        self.set_secure_cookie("user", tornado.escape.json_encode(user))
        self.redirect(self.reverse_url("main"))


class GoogleAuthHandler(BaseHandler, tornado.auth.GoogleMixin):

    @tornado.web.asynchronous
    def get(self):
        if self.get_argument("openid.mode", None):
            self.get_authenticated_user(self.async_callback(self._on_auth))
            return
        self.authenticate_redirect()

    def _on_auth(self, user):
        if not user:
            raise tornado.web.HTTPError(500, "Google auth failed")
        user["auth"] = "google"
        self.set_secure_cookie("user", tornado.escape.json_encode(user))
        self.redirect(self.reverse_url("main"))


class LogoutHandler(BaseHandler):

    def get(self):
        self.clear_cookie("user")
        self.write('Logged out. <a href="%s">Log back in</a>.' % self.reverse_url("auth"))


class MainHandler(BaseHandler):

	@tornado.web.authenticated
	def get(self):
		res = self.mongo_src.things.find()
		self.render("base.html")


class ProfileHandler(BaseHandler):

    @tornado.web.authenticated
    def get(self):
        user = self.get_current_user()
        self.render("accounts/profile.html", user=user)


class Http404Handler(BaseHandler):

    def get(self):
        self.render("http404.html")

import os
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options

from tornado.options import define, options

from backend.handlers import MainHandler
from backend.handlers import AuthHandler
from backend.handlers import GoogleAuthHandler
from backend.handlers import FacebookAuthHandler
from backend.handlers import LogoutHandler
from backend.handlers import ProfileHandler
from backend.handlers import Http404Handler

from backend.storages import MongoSource
from backend.storages import RedisSource


define("port", default=8000, help="run on the given port", type=int)


class Application(tornado.web.Application):

	def __init__(self, settings):

		handlers = [
			tornado.web.url(r'/', MainHandler, name="main"),
			tornado.web.url(r'/auth/$', AuthHandler, name="auth"),
			tornado.web.url(r'/auth/google/$', GoogleAuthHandler, name="auth_google"),
			tornado.web.url(r'/auth/facebook/$', FacebookAuthHandler, name="auth_facebook"),
			tornado.web.url(r'/logout/$', LogoutHandler, name="logout"),
			tornado.web.url(r'/profile/$', ProfileHandler, name="profile"),
			tornado.web.url(r'.*', Http404Handler, name='http404')
		]

		tornado.web.Application.__init__(self, handlers, **settings)


def run():
	settings = dict(
        cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        login_url="/auth/",
        template_path=os.path.join(os.path.dirname(__file__), os.path.join("frontend", "templates")),
        static_path=os.path.join(os.path.dirname(__file__), os.path.join("frontend", "static")),
        xsrf_cookies=True,
        autoescape="xhtml_escape",
        facebook_api_key="100432673463557",
        facebook_secret="da194f5e2fca9a4c5ca81ccd09a90580",
        debug=True,
        use_mongo=True,
        use_redis=False
	)
	tornado.options.parse_command_line()	
	app = Application(settings)
	if settings.get('use_mongo', False):
		MongoSource.setup(settings)
	if settings.get('use_redis', False):
		RedisSource.setup(settings)
	server = tornado.httpserver.HTTPServer(app)
	server.listen(options.port)
	if settings.get('debug', False):
		tornado.autoreload.start()
	tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
	run()
